## Observações
Você deverá fazer um **Fork Privado** desse projeto, implementar a solução de acordo com o problema abaixo e nos enviar o repositório para avaliação.

## Descrição Problema:

Hoje para vendermos nosso curso, precisamos que esses cursos sejam ofertados para nosso e-commerce. Após a conclusão da compra precisamos iniciar a vida do aluno em nossas aplicações. Com isso, você foi convidado para implementar uma API onde você deve cadastrar, fornecer e desativar as ofertas necessárias para serem exibidas no e-commerce e também receber as matrículas que descerão do e-commerce via API. Após ter recebido a matrícula, é importante que essa matrícula esteja disponível para outros sistemas utilizarem em processos acadêmicos e financeiros, além de outros sistemas.

Para a comunicação com a API, é necessário utilizar o protocolo Http. Já a publicação para outros sistemas, será através de mensageria utilizando Kafka.

Você terá a liberdade de implementar no pattern que você julga o correto, dado o contexto.

Obs: para cadastrar as informações elas necessitarão serem validadas antes

**Data-Mapping Oferta**

| Name | Type |
| --- | --- |
| Id | Numeral |
| Data de Inicio | Data e Hora |
| Data fim da oferta | Data e hora |
| Ativo | Boolean |

**Data-Mapping Pessoa**

| Name | Type |
| --- | --- |
| Id | Numeral |
| Nome | Texto |
| CPF | Texto |
| e-mail | Texto |

**Data-Mapping Matrícula**

| Name | Type |
| --- | --- |
| Id | Numeral |
| Id da Pessoa | Numeral |
| Id da Oferta | Numeral |
| Data de matrícula | Data e hora |

### Requisitos:

- Criar o Dockerfile para execução local.

- Utilizar um dos frameworks: Spring ou Micronaut.

- Necessário testes Unitários.

- Documentação da implementação.


